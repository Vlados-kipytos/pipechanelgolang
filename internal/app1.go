package internal

import (
	"AwesomeProject/internal/core"
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"
)

func SendJson() {
	messages := make([]core.Message, 0)
	messages = append(messages, core.Msg1, core.Msg2, core.Msg3)

	file, err := os.OpenFile("logs/app1.log", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Print(err)
	}

	i := 0
	for {
		rand.Seed(time.Now().Unix()) // initialize global pseudo random generator

		msg := messages[rand.Intn(len(messages))]

		buf, err := json.Marshal(msg)

		if err != nil {
			log.Fatal(err)
		}

		switch msg.Type {
		case core.TypeRun, core.TypeStay, core.TypeJump:
			if err := ioutil.WriteFile("channels/channel1.pipe", buf, 0644); err != nil {
				_, err := file.Write([]byte(err.Error()))
				if err != nil {
					log.Fatal(err)
				}
			}
			_, err = file.Write(buf)
			if err != nil {
				log.Fatal(err)
			}
		default:
			log.Fatalf("unknown message type: %q", msg.Type)
		}

		time.Sleep(time.Second)
		i++
	}

}

package internal

import (
	"AwesomeProject/internal/core"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)


func ReadApp1() {
	var SummaryMessage = core.Message{}

	fmt.Println("start schedule writing.")
	file, err := os.OpenFile("logs/app2.log", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {log.Fatalf("error opening file: %v", err)}

	i := 0
	for {

		MessageIn, err := ioutil.ReadFile("channels/channel1.pipe")
		if err != nil {log.Fatal("Cannot load settings:", err)
		}

		_ = json.Unmarshal(MessageIn, &SummaryMessage)

		if SummaryMessage.Type == core.Msg3.Type {

			_, err = file.Write(MessageIn);if err != nil {log.Fatal(err)}

		} else {

			if err := ioutil.WriteFile("channels/channel2.pipe", MessageIn, 0644); err != nil {
				_, err := file.Write([]byte(err.Error()));if err != nil {log.Fatal(err)}
			}

			time.Sleep(time.Second)
			i++

		}
	}
}

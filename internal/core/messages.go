package core

type Message struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}

type PayloadRun struct {
	Time  int `json:"time"`
	Speed int `json:"speed"`
}

type PayloadJump struct {
	Height int `json:"height"`
}

type PayloadStay struct {
	Time int `json:"time"`
}

func NewMessageRun(payload PayloadRun) Message {
	return Message{Type: TypeRun, Payload: payload}
}

func NewMessageJump(payload PayloadJump) Message {
	return Message{Type: TypeJump, Payload: payload}
}

func NewMessageStay(payload PayloadStay) Message {
	return Message{Type: TypeStay, Payload: payload}
}
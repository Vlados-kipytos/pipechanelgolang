package internal

import (
	"AwesomeProject/internal/core"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)


func ReadApp2() {
	var SummaryMessage = core.Message{}

	file, err := os.OpenFile("logs/app3.log", os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {log.Fatalf("error opening file: %v", err)}

	for {

		MessageIn, err := ioutil.ReadFile("channels/channel2.pipe")
		if err != nil {log.Fatal("Cannot load settings:", err)}

		_ = json.Unmarshal(MessageIn, &SummaryMessage)

		fmt.Print(SummaryMessage.Type)

		if SummaryMessage.Payload != nil{
			_, err = file.Write(MessageIn);if err != nil {log.Fatal(err)}
		}

		time.Sleep(time.Second)

	}

}
